'use strict';

const gulp = require('gulp'),
    git = require('gulp-git'),
    sass = require('gulp-sass'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    gulpJshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    paths = {
        js: ['app.js', 'lib/js/*'],
        sass: 'lib/sass/*'
    };

const dist = gulp.series(_distJs, _distScss);

module.exports = {
    default: dist,
    dist,
    jshint
}

function _distJs() {
    return gulp.src(paths.js)
        .pipe(concat('sp-autocomplete.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-autocomplete.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function _distScss() {
    return gulp.src(paths.sass)
        .pipe(sass())
        .pipe(concat('sp-autocomplete.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(cleanCSS({specialComments: 0}))
        .pipe(rename('sp-autocomplete.min.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function jshint() {
    return gulp.src(paths.js)
        .pipe(gulpJshint({
            latedef: 'nofunc',
            validthis: true,
            quotmark: false,
            curly: false
        }))
        .pipe(gulpJshint.reporter(stylish));
}