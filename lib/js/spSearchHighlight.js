
(function () {
    'use strict';

    angular.module('sp-autocomplete').directive('spSearchHighlight', [
        '$rootScope', 'Util',
        function ($rootScope, util) {
            return {
                restrict: 'E',
                scope: {
                    query: '=',
                    option: '='
                },
                link: function ($scope, $element) {
                    function setHighlight() {
                        if (!$scope.option) return;
                        if (!$scope.query) {
                            $element.html($scope.option);
                            return;
                        }

                        $element.html($scope.option.replace(new RegExp($scope.query.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'gi'), '<span class="sp-search-highlight">$&</span>'));
                    }

                    setHighlight();

                    util.destroyListeners($scope, $rootScope.$on('autoComplete.set', setHighlight));
                }
            };
        }]);

}());